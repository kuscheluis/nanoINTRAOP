configfile: "config.yaml"

# Define subworkflow to access data from the the nanoDx intraop branch
subworkflow nanoDx:
    workdir:
        "../nanoDx_intraop"
    configfile:
        "../nanoDx_intraop/config.yaml"

# Create a summary for all samples in config.yaml based on choosen trainingsset
rule summaryClassification:
    input:
        RData = nanoDx(expand("classification/{sample}-votes-{{trainingset}}.RData", sample=config["samples"])),
        QC = expand("stats/{sample}.nanostat.txt", sample=config["samples"])
    output: "summaryClassification_{trainingset}.tsv"
    conda: nanoDx("envs/basicR.yaml")
    script: "scripts/summaryClassification.R"

# Read extraction 1: Extract read IDs with timepoints and sequence length from the FASTQ-file per sample
rule extract_readIDs_with_timepoints_and_sequence_length:
    input: nanoDx("fastq/{sample}.fq")
    output:
        timepoints = "tables/timepoints_{sample}.tsv",
        seqlength = "tables/seqlength_{sample}.tsv"
    conda: "envs/bioawk.yaml"
    shell:
        """
        awk 'sub(/^@/,"") && sub(/ .*start_time=/,"\t")' {input} > {output.timepoints}
        bioawk -cfastx '{{print $name, length($seq)}}' {input} > {output.seqlength}
        """

# Read extraction 2: Return read IDs for choosen timeframe per sample with timepoints and extract only read IDs without timepoints
rule return_readIDs_per_timeframe:
    input: "tables/timepoints_{sample}.tsv"
    output: "timeframe/{sample}_{timeframe}.tsv"
    conda: nanoDx("envs/basicR.yaml")
    script: "scripts/read_ID_timeframe_extraction.R"

rule extract_only_readIDs_per_timeframe:
    input: "timeframe/{sample}_{timeframe}.tsv"
    output: "readIDs/{sample}_{timeframe}.tsv"
    shell: """awk -F "\t" '{{print $2}}' {input} | awk '{{print substr($0,2,length()-2);}}' > {output}"""

# UNDER CONSTRUCTION: Read extraction 3: Extract timeframes from methylation call based on read IDs in timeframe
rule extract_timeframes_from_methylation_call:
    input:
        calls = nanoDx("methylation/{sample}.calls"),
        readIDs = "readIDs/{sample}_{timeframe}.tsv"
    output: "methylation/{sample}_{timeframe}.calls"
    shell:
        "head -1 {input.calls} > {output} ; "
        "grep -w -F -f {input.readIDs} {input.calls} >> {output}"

# Plot CpG (overlap) yield against base yield for all samples
rule QC_nanostat:
    input:
        bam = nanoDx("bam/{sample}.bam"),
        bai = nanoDx("bam/{sample}.bam.bai")
    output:
        "stats/{sample}.nanostat.txt"
    threads: 1
    conda: "envs/qc.yaml"
    shell:
        "NanoStat --tsv --bam {input.bam} > {output}"

rule CpG_overlap_vs_base_yield:
    input: "summaryClassification_Capper_et_al.tsv"
    output: "plots/Figure1.pdf"
    conda: nanoDx("envs/basicR.yaml")
    script: "scripts/CpG_overlap_vs_base_yield.R"
