# Intraoperative DNA methylation classification of brain tumors impacts neurosurgical strategy - Djirackor et al.

Analysis pipeline for the "Intraoperative DNA methylation classification of brain tumors impacts neurosurgical strategy" publication from Djirackor et al. 
This pipeline uses the nanoDx pipeline as a subworkflow, which should be located next to the nanoINTRAOP analysis pipeline to function properly.

Link to the nanoDx pipeline: https://gitlab.com/pesk/nanoDx/

# Reference
If you use this analysis pipeline, please consider citing our publication:

Luna Djirackor, Skarphedinn Halldorsson, Pitt Niehusmann, Henning Leske, David Capper, Luis P. Kuschel, Jens Pahnke, Bernt J. Due-Tønnessen, Iver A. Langmoen, Cecilie J. Sandberg, Philipp Euskirchen, and Einar O. Vik-Mo. 2021. „Intraoperative DNA Methylation Classification of Brain Tumors Impacts Neurosurgical Strategy“. Neuro-Oncology Advances 3(1):vdab149. doi: 10.1093/noajnl/vdab149.

Link to the publication: https://doi.org/10.1093/noajnl/vdab149
